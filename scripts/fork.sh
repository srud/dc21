#!/bin/sh
set -euf

name=$1

git clone --single-branch --local . ../$name/
cd ../$name/
git branch
git filter-branch --index-filter '
    git rm --cached --ignore-unmatch -r \
      "assets/docs/*.pdf" \
      "assets/img/*.jpg" \
      "assets/img/*.png" \
      "assets/img/*.svg" \
      "assets/img/favicon/*.ico" \
      "assets/img/favicon/*.png" \
      "assets/img/favicon/*.svg" \
      "assets/video/*.jpg" \
      "assets/video/*.mp4" \
      "sponsors/*.png" \
      "sponsors/*.svg" \
      "sponsors/*.yml" \
      "static/"
  ' HEAD
git remote rm origin
rm -rf .git/refs/remotes
git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
git reflog expire --expire=now --all
git gc --prune=now --aggressive
