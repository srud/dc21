import datetime
from django.conf import settings
from django.utils import timezone

def expose_wafer_talks(request):
    return {'WAFER_TALKS_OPEN': settings.WAFER_TALKS_OPEN}


def is_it_debconf(request):
    today = timezone.now().date()
    two_days = datetime.timedelta(days=2)
    start = min([e[1] for e in settings.DEBCONF_DATES])
    end = max([e[2] for e in settings.DEBCONF_DATES])
    return {
        'debconf_day': today >= start and today <= end,
        'debconf_soon': today < start and (today >= start - two_days),
    }
